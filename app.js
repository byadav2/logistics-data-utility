var express = require('express');
let fetch = require('node-fetch');
var os = require('os');
const axios = require('axios');
const fs = require('fs');
const https = require('https');
const util = require('util');

var app = express();
const readdir = util.promisify(fs.readdir);
const readFile = util.promisify(fs.readFile);

const readFiles = async dirname => {
    try {
        const filenames = await readdir(dirname);
        //console.log({ filenames });
        const files_promise = filenames.map(filename => {
            return readFile(dirname + '/' + filename, 'utf-8');
        });
        const response = await Promise.all(files_promise);
        //console.log({ response })
        //return response

        return filenames.reduce((accumlater, filename, currentIndex) => {
            let tpIds = new Set();
            const content = response[currentIndex];
                contentArr = content.split("\n");

                firstLine = contentArr[0];
                envArr = firstLine.split("env: ");
                tpIds.add(envArr[1]);

                for (var i in contentArr) {
                  var tp = contentArr[i].match("Traversal Path (.*) is created in available true status");
                  if (tp != null) {
                    tpIds.add(tp[1]);
                  }
                }

                accumlater[filename] = Array.from(tpIds);
                return accumlater;
        }, {});
    } catch (error) {
        console.error(error);
    }
};

app.get('/api/v1/tps', async function (req, res) {
	const response = await readFiles(
            './log4j',
        );
    //console.log({ response });
	res.setHeader('Content-Type', 'application/json')
	res.send(response);
});

app.listen(3000, function () {
  console.log('Logistics-data-utility app listening on port 3000!');
});
