# logistics-data-utility

## Getting started

### Prerequisite
```
1. Clone repo:
    https://gitlab.com/byadav2/logistics-data-utility
2. Install NodeJs:
    https://nodejs.org/en/download/
```

### Running app:
```
1. npm install
2. node app
3. Run 
    curl --location --request GET 'http://localhost:3000/api/v1/tps'
```
